'use client';
import type { RefObject } from 'react';
import { useCallback, useEffect } from 'react';

import noop from 'utils/noop';

type Handler = (event: MouseEvent) => void;
type Event = 'mousedown' | 'mouseup';

function useOutsideClick<T extends HTMLElement = HTMLElement>(
  ref: RefObject<T>,
  handler: Handler = noop,
  mouseEvent: Event = 'mousedown',
): void {

  const listener = useCallback((event: MouseEvent) => {
    const el = ref?.current

    if (!el || el.contains(event.target as Node)) {
      return
    }

    handler(event)
  }, [handler, ref])

  useEffect(() => {
    document.addEventListener(mouseEvent, listener);

    return () => {
      window.removeEventListener(mouseEvent, listener);
    };
  }, [listener, mouseEvent])

}

export default useOutsideClick
