export enum SortTypes {
  Name = 'name',
  Price = 'price',
}

export enum SortOrder {
  ASC = 'asc',
  DESC = 'desc',
}
