export enum CurrencyOptions {
  USD = '$',
  EUR = '€',
  GBP = '£',
  AMD = '֏',
}
