const BASE_ICONS_URL = 'https://firebasestorage.googleapis.com/v0/b/bejamas-77152.appspot.com/o/icons%2F'

export const LOGO = `${BASE_ICONS_URL}logo.svg?alt=media`;
export const CART = `${BASE_ICONS_URL}cart.svg?alt=media`;
export const SORTING = `${BASE_ICONS_URL}sorting.svg?alt=media`;
export const ARROW_DOWN = `${BASE_ICONS_URL}arrow-down.svg?alt=media`;
export const ARROW_LEFT = `${BASE_ICONS_URL}arrow-left.svg?alt=media`;
export const ARROW_RIGHT = `${BASE_ICONS_URL}arrow-right.svg?alt=media`;
export const CLOSE = `${BASE_ICONS_URL}close.svg?alt=media`;
export const FILTER = `${BASE_ICONS_URL}filter.svg?alt=media`;
export const CHECKBOX = `${BASE_ICONS_URL}checkbox.svg?alt=media`;
export const CHECKBOX_CHECKED = `${BASE_ICONS_URL}checkbox-checked.svg?alt=media`;
