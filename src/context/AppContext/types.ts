import type { Dispatch, ReactNode, SetStateAction } from 'react';
import type { ProductProps } from 'types/product';

export type AppContextDataProps = {
  isShoppingCartOpen: boolean;
  shoppingCartItems: ProductProps[];
  setIsShoppingCartOpen: Dispatch<SetStateAction<boolean>>;
  setShoppingCartItems: Dispatch<SetStateAction<ProductProps[]>>
}

export type AppContextProviderProps = {
  children: ReactNode
}


