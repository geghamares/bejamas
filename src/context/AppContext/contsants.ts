import type { AppContextDataProps } from './types';
import noop from 'utils/noop';

const appContextDefaultValue: AppContextDataProps= {
  isShoppingCartOpen: false,
  shoppingCartItems: [],

  setShoppingCartItems: noop,
  setIsShoppingCartOpen: noop,
}

export default appContextDefaultValue;
