'use client';
import { createContext, FC, useContext, useState } from 'react';
import type { AppContextDataProps, AppContextProviderProps } from './types';
import type { ProductProps } from 'types/product';
import appContextDefaultValue from './contsants';

const AppContext = createContext<AppContextDataProps>(appContextDefaultValue);

export const AppProvider: FC<AppContextProviderProps> = ({ children }) => {
  const [isShoppingCartOpen, setIsShoppingCartOpen] = useState(false);
  const [shoppingCartItems, setShoppingCartItems] = useState<ProductProps[]>([]);

  const appContextValue: AppContextDataProps = {
    isShoppingCartOpen,
    setIsShoppingCartOpen,
    shoppingCartItems,
    setShoppingCartItems,
  };

  return (
    <AppContext.Provider value={appContextValue}>
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = () => useContext(AppContext);
