export type ImageProps = {
  src: string;
  alt: string;
}

export type BaseDimensionProps = {
  width: number;
  height: number;
};

