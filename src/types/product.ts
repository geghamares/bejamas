import type { BaseDimensionProps, ImageProps } from './general';
import type { CurrencyOptions } from 'constants/general';

export type ProductProps = {
  name: string;
  category: string;
  price: number;
  currency: keyof typeof CurrencyOptions;
  image: ImageProps;
  bestseller?: boolean;
  featured?: boolean;
  details: ProductDetails;
  dimensions?: BaseDimensionProps;
}

export type ProductDetails = {
  dimensions: BaseDimensionProps;
  size: number;
  description?: string;
  recommendations?: ImageProps[];
}
