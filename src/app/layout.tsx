import './globals.css'
import type { ReactNode } from 'react';

export default function RootLayout({
  children,
}: {
  children: ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <title>Bejamas | Tech test</title>
      </head>
      <body>
        {children}
        <div id="portal" />
      </body>
    </html>
  )
}
