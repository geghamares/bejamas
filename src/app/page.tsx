import dynamic from 'next/dynamic';

import Banner from 'organisms/Banner';
import Header from 'organisms/Header';
import type { ProductProps } from 'types/product';
import getDocuments, { getFeaturedProduct } from 'utils/getProducts';

const ProductsSection = dynamic(() => import('organisms/ProductsSection'));
const AppProvider = dynamic(() => import('context/AppContext'));
const Divider = dynamic(() => import('atoms/Divider'));

export default async function Home() {
  const result = await getDocuments({});
  const featuredProduct = await getFeaturedProduct();

  return (
    <AppProvider>
      <Header />
      <Banner {...featuredProduct as ProductProps} />
      <Divider />
      <ProductsSection initialProducts={result.data as ProductProps[]} title="Photography" subtitle="Premium Photos" />
    </AppProvider>
  )
}
