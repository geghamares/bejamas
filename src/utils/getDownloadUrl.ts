const BASE_STORAGE_URL = 'https://firebasestorage.googleapis.com/v0/b/bejamas-77152.appspot.com/o/';
const DOWNLOAD_ALT = '?alt=media';

export const getDownloadUrl = (folder: string, name: string) => `${BASE_STORAGE_URL}${folder}%2F${name}${DOWNLOAD_ALT}`;
