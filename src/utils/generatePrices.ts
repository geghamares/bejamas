import type { PriceRange } from 'organisms/ProductsSection/types';
import type { ProductProps } from 'types/product';

function generatePriceRanges(products: ProductProps[], incrementCount = 4): PriceRange[] {
  const prices = products.map(product => product.price);
  const maxPrice = Math.max(...prices);

  // Calculate the average increments
  const maxIncrementSize = Math.ceil(maxPrice / incrementCount / 100) * 100;
  const incrementSize = maxIncrementSize / incrementCount;

  const ranges: PriceRange[] = [];

  // Generate ranges based on the average increments
  let currentMin = 0;
  for (let i = 0; i < incrementCount; i++) {
    const currentMax = currentMin + incrementSize;

    if (currentMin === 0) {
      // Add the range for prices lower than first average
      ranges.push({
        range: { min: currentMin, max: currentMax },
        label: `Less than $${currentMax.toFixed(0)}`
      });
    } else {
      ranges.push({
        range: { min: currentMin, max: currentMax },
        label: `$${currentMin.toFixed(0)} - $${currentMax.toFixed(0)}`
      });
    }
    currentMin = currentMax;
  }

  // Add the range for prices higher than the maximum price
  ranges.push({
    range: { min: maxPrice, max: Number.MAX_VALUE },
    label: `More than $${maxPrice.toFixed(2)}`
  });

  return ranges;
}

export default generatePriceRanges;
