export default (str: string) => str.replace(/^\w/, match => match.toUpperCase());
