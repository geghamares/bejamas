import {
  collection,
  getCountFromServer,
  getDocs,
  limit as l,
  orderBy,
  query,
  startAfter,
  where,
  getDoc,
} from 'firebase/firestore';
import { db } from 'src/firebase/config';
import { SortOrder, SortTypes } from 'constants/fetch';

const ProductsRef = collection(db, 'Prodcuts');

export type RangeProps = {
  min: number;
  max: number;
}

export type GetDocumentArgs = {
  sortType?: SortTypes;
  isAscending?: boolean;
  categories?: string[];
  priceRange?: RangeProps | null;
  lastVisible?: any,
  limit?: number,
}

const getDocuments = ({
  sortType = SortTypes.Name,
  isAscending = false,
  categories,
  priceRange,
  lastVisible = null,
  limit,
}: GetDocumentArgs) => {
  let q = query(ProductsRef, orderBy(sortType, isAscending ? SortOrder.ASC : SortOrder.DESC));

  if (limit) {
    q = query(q, l(limit));
  }

  if (categories) {
    q = query(q, where('category', 'in', categories));
  }

  if (priceRange) {
    q = query(q, where('price', '>=', priceRange.min), where('price', '<=', priceRange.max));
  }

  if (lastVisible) {
    q = query(q, startAfter(lastVisible));
  }

  const stapshot = getDocs(q).then(querySnapshot => {
    const data = querySnapshot.docs.map((doc) => doc.data());
    return {
      data,
      count: data.length,
    }
  })

  return stapshot;
}

export const getFeaturedProduct = () => {
  const q = query(ProductsRef, where('featured', '==', true));

  return getDocs(q).then(querySnapshot => {
    const data = querySnapshot.docs.map((doc) => doc.data());
    return data[0];
  })
}

export const getDocumentsCount = async (): Promise<number> => {
  const snapshot = await getCountFromServer(ProductsRef);

  return snapshot.data().count;
}

export default getDocuments;
