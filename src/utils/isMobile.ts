function isMobile() {
  return typeof navigator !== 'undefined' && /Mobi|Android/i.test(navigator.userAgent);
}

export default isMobile;
