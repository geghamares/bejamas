'use client';
import { ComponentPropsWithRef, ElementType, forwardRef, Ref } from 'react';

import SVGIcon from 'atoms/SVGIcon';
import capitalizeFirstLetter from 'utils/capitalizeFirstLetter';
import { CHECKBOX, CHECKBOX_CHECKED } from 'constants/icons';

import type { ICheckbox } from './types';
import styles from './Checkbox.module.css'

function Checkbox({
  isChecked,
  label,
  onChange,
  type = 'checkbox',
  value,
  ...props
}: ICheckbox) {

  return (
    <label
      data-testid="input-wrapper"
      className={styles.root}
    >
      <span
        className={styles.checkbox}
      >
        { isChecked
          ? <SVGIcon className={styles.icon} src={CHECKBOX_CHECKED} alt="" />
          : <SVGIcon className={styles.icon} src={CHECKBOX} alt="" />
        }
        <input
          className={styles.control}
          data-testid="input-controller"
          checked={isChecked}
          tabIndex={0}
          type={type}
          value={value || label}
          onChange={onChange}
          {...props as ComponentPropsWithRef<ElementType>}
        />
      </span>
      {label && <span className={styles.label}>{capitalizeFirstLetter(label)}</span>}
    </label>
  );
}

export default Checkbox;
