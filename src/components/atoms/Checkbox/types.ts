import { ChangeEvent, HTMLProps } from 'react';

export interface ICheckbox extends Omit<HTMLProps<HTMLInputElement>, 'size' | 'ref'> {
  className?: string;
  isChecked?: boolean;
  isDisabled?: boolean;
  isDefaultChecked?: boolean;
  tabIndex?: number;
  label?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  id?: string;
}
