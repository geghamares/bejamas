export type SVGIconProps = {
  src: string;
  alt: string;
  width?: number;
  height?: number;
  className?: string;
}
