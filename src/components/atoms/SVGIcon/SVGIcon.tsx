import type { SVGIconProps } from './types';

function SVGIcon({ src, alt, width = 24, height = 24, className }: SVGIconProps) {
  return <img className={className} src={src} alt={alt} width={width} height={height} />;
}

export default SVGIcon;
