import type { ButtonVariants } from './constants';

export type ButtonProps = {
  onClick: () => void;
  variant?: ButtonVariants;
  label: string;
  className?: string;
}
