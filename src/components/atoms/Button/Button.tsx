'use client';

import React from 'react';
import classNames from 'classnames';

import { ButtonVariants } from './constants';
import type { ButtonProps } from './types';
import styles from './Button.module.css';

function Button({
  variant = ButtonVariants.Primary,
  label,
  onClick,
  className = '',
}: ButtonProps){

  return (
    <button onClick={onClick} className={classNames(className, styles.cta, styles[variant])}>
      {label}
    </button>
  );
}

export default Button;
