'use client';
import { useRef, useEffect, useState } from 'react'
import { createPortal } from 'react-dom'
import classNames from 'classnames';

import SVGIcon from 'atoms/SVGIcon';
import useOutsideClick from 'hooks/useOutsideClick';
import noop from 'utils/noop';
import { CLOSE } from 'constants/icons';

import { PopupPositions } from './constants';
import type  { PopupProps } from './types';
import styles from './Popup.module.css'

function Popup({
  children,
  position = PopupPositions.Center,
  isOpen = false,
  onClose,
  enableOutsideClick = false,
  className,
}: PopupProps){
  const portalRef = useRef<HTMLElement | null>(null);
  const contentRef = useRef<HTMLDivElement | null>(null);
  const [mounted, setMounted] = useState(false);


  useEffect(() => {
    portalRef.current = document.getElementById('portal');
    setMounted(true)
  }, [])

  useOutsideClick(contentRef, enableOutsideClick && onClose ? onClose : noop)

  if (mounted && portalRef.current && isOpen) {
    return createPortal(<div className={styles.root}>
      <div ref={contentRef} className={classNames(styles.content, className, styles[position])}>
        {onClose && (
          <button className={styles.close} onClick={onClose}>
            <SVGIcon src={CLOSE} alt="close" />
          </button>
        )}
        {children}
      </div>
    </div>, portalRef.current)
  }

  return null;
}

export default Popup;
