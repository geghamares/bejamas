import type { ReactNode } from 'react';
import type { PopupPositions } from './constants';

export type PopupProps = {
  children: ReactNode;
  isOpen?: boolean;
  onClose?: () => void;
  position?: PopupPositions;
  enableOutsideClick?: boolean;
  className?: string;
}
