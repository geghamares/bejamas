import Image from 'next/image';

import { CurrencyOptions } from 'constants/general';
import type { ProductProps } from 'types/product';

import styles from './CartItem.module.css';

function CartItem({ name, price, currency, image }: ProductProps) {
  return (
    <div className={styles.root}>
      <div className={styles.textContainer}>
        <h3 className={styles.name}>{name}</h3>
        <span className={styles.price}>{`${CurrencyOptions[currency]}${price}`}</span>
      </div>
      <Image className={styles.image} src={image.src} alt={image.alt} width={148} height={86} />
    </div>
  )
}

export default CartItem;
