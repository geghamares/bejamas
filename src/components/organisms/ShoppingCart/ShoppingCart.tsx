'use client';

import { useAppContext } from 'context/AppContext';
import CartItem from 'organisms/ShoppingCart/CartItem';
import SVGIcon from 'atoms/SVGIcon';
import Button from 'atoms/Button';
import { PopupPositions } from 'atoms/Popup/constants';
import { CART, CLOSE } from 'constants/icons';
import type { ProductProps } from 'types/product';

import { ButtonVariants } from 'atoms/Button/constants';
import styles from './ShoppingCart.module.css';

function ShoppingCart() {
  const { isShoppingCartOpen, setIsShoppingCartOpen, shoppingCartItems, setShoppingCartItems } = useAppContext()

  return (
    <div className={styles.root}>
      <button className={styles.cartWrapper} onClick={() => setIsShoppingCartOpen(prev => !prev)}>
        <SVGIcon className={styles.cart} src={CART} alt="cart" width={54} height={54} />
        {shoppingCartItems.length > 0 && <span className={styles.counter}>{shoppingCartItems.length}</span>}
      </button>
      {isShoppingCartOpen && (
        <div className={styles.popup}>
          <button className={styles.close} onClick={() => setIsShoppingCartOpen(false)}>
            <SVGIcon src={CLOSE} alt="close" />
          </button>
          {shoppingCartItems.length > 0 && (
            <div className={styles.itemsWrapper}>
              <div className={styles.items}>
                {shoppingCartItems.map((item: ProductProps) => <CartItem key={`cart-item-${item.name}`} {...item} />)}
              </div>
              <div className={styles.clear}>
                <Button variant={ButtonVariants.Secondary} label="CLEAR" onClick={() => setShoppingCartItems([])} />
              </div>
            </div>
          )}
          {!shoppingCartItems.length && (
            <p className={styles.emptyCart}>Your cart is empty</p>
          )}
        </div>
      )}

    </div>
  )
}

export default ShoppingCart;
