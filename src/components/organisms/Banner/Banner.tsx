import Image from 'next/image';

import AddToCardButton from 'molecules/AddToCardButton';

import type { ProductProps } from 'types/product';
import type { ImageProps } from 'types/general';

import styles from './Banner.module.css';

function Banner(props: ProductProps) {

  const {
    name,
    image,
    category,
    details: {
      dimensions,
      size,
      description,
      recommendations
    },
  } = props;

  return (
    <div className={styles.root}>
      <h1 className={styles.title}>{name}</h1>
      <div className={styles.buttonHolder}>
        <AddToCardButton {...props} />
      </div>
      <div className={styles.mediaWrapper}>
        <Image className={styles.image} src={image.src} alt={image.alt} width={1290} height={554} sizes="(max-width: 768px) 346px" priority />
        <span className={styles.featured}>Photo of the day</span>
      </div>
      <div className={styles.infoWrapper}>
        <h2 className={styles.subtitle}>About the {name}</h2>
        <span className={styles.category}>{category}</span>
        {description && <p className={styles.description}>{description}</p>}
      </div>
      {recommendations && (
        <div className={styles.recommendations}>
          <span className={styles.subtitle}>People also buy</span>
          <div className={styles.recommendedImages}>
            {recommendations.map(({ src, alt }: ImageProps) => <Image key={src} src={src} alt={alt} width={118} height={148} />)}
          </div>
        </div>
      )}
      <div className={styles.details}>
        <span className={styles.subtitle}>Details</span>
        <span className={styles.detail}>Dimensions: {dimensions.width} x {dimensions.height} pixel</span>
        <span className={styles.detail}>Size: {Math.ceil(size / 1024)} mb</span>
      </div>
    </div>
  );
}

export default Banner;
