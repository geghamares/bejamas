import dynamic from 'next/dynamic';
import classNames from 'classnames';

import SVGIcon from 'atoms/SVGIcon';
import { LOGO } from 'constants/icons';

import styles from './Header.module.css';

const ShoppingCart = dynamic(() => import('organisms/ShoppingCart/ShoppingCart'), {ssr: false});


function Header() {
  return (
    <header className={styles.root}>
      <div className={styles.wrapper}>
        <SVGIcon className={classNames(styles.image, styles.logo)} src={LOGO} alt='logo' width={160} height={26} />
        <ShoppingCart />
      </div>
    </header>
  );
}

export default Header;
