'use client';
import { useEffect, useMemo, useState } from 'react';
import dynamic from 'next/dynamic';

import ProductItem from 'molecules/ProductItem';
import getDocuments, { GetDocumentArgs } from 'utils/getProducts';
import type { ProductSectionProps } from 'organisms/ProductsSection/types';
import type { ProductProps } from 'types/product';

import generatePriceRanges from 'utils/generatePrices';
import isMobile from 'utils/isMobile';
import styles from './ProductsSection.module.css';

const CategoryFiltering = dynamic(() => import('./CategoryFiltering'), { ssr: false });
const PriceFiltering = dynamic(() => import('./PriceFiltering'), { ssr: false });
const Pagination = dynamic(() => import('molecules/Pagination'), { ssr: false });
const Sorting = dynamic(() => import('./Sorting'), { ssr: false });
const Divider = dynamic(() => import('atoms/Divider'), { ssr: false });

const getArr = (props: ProductProps[]) => {
  const arr: string[] = [];
  props.forEach(item => {
    if (!arr.includes(item.category)) {
      arr.push(item.category);
    }
  });

  return arr;
}

function ProductsSection({ initialProducts, title, subtitle }: ProductSectionProps) {
  const [queries, setQueries] = useState<GetDocumentArgs>({} as GetDocumentArgs)

  const [initiallyLoaded, setInitiallyLoaded] = useState(false);
  const [count, setCount] = useState(initialProducts.length);
  const [itemsPerPage, setItemsPerPage] = useState(6);
  const [totalPages, setTotalPages] = useState(Math.ceil(count / itemsPerPage))
  const [currentPage, setCurrentPage] = useState(1);

  const [clientData, setClientData] = useState<ProductProps[]>([]);
  const [currentData, setCurrentData] = useState<ProductProps[]>(initialProducts.slice(
    (currentPage - 1) * itemsPerPage,
    currentPage * itemsPerPage
  ));

  const handleQueryUpdate = (obj: GetDocumentArgs) => setQueries(prevState => ({...prevState, ...obj}));

  const fetchClientData = async (q: GetDocumentArgs) => {
    const docs = await getDocuments(q)
    setClientData(docs.data as ProductProps[]);
    setCount(docs.count);
  };

  //const checkboxes = useMemo(() => initialProducts.map((product) => ({ label: product.category, isChecked: false })), [initialProducts]);
  const checkboxes = useMemo(() => getArr(initialProducts), [initialProducts]);
  const prices = useMemo(() => generatePriceRanges(initialProducts), [initialProducts]);

  useEffect(() => {
    if(queries && Object.keys(queries).length) {
      fetchClientData(queries);
    }
  }, [queries])

  useEffect(() => {
    setInitiallyLoaded(true);

    if(isMobile()) {
      setItemsPerPage(4);
    }
  }, [])

  useEffect(() => {
    setTotalPages(Math.ceil(count / itemsPerPage));
    if (clientData.length) {
      setCurrentData(clientData.slice(
        (currentPage - 1) * itemsPerPage,
        currentPage * itemsPerPage
      ))
    } else {
      setCurrentData(initialProducts.slice(
        (currentPage - 1) * itemsPerPage,
        currentPage * itemsPerPage
      ))
    }
  }, [count, clientData, currentPage, initialProducts, itemsPerPage])

  return (
    <div className={styles.root}>
      <div className={styles.heading}>
        <h2 className={styles.title}>{title}  /</h2>
        <span className={styles.subtitle}>{subtitle}</span>
      </div>
      <div className={styles.sorting}>
        <Sorting values={queries.priceRange ? ['Price'] : ['Name', 'Price']} onUpdate={handleQueryUpdate} isAscending={Boolean(queries.isAscending)} />
      </div>
      <div className={styles.filtering}>
        <CategoryFiltering label="Categories" checkboxValues={checkboxes} onUpdate={handleQueryUpdate} />
        <Divider />
        <PriceFiltering label="Price range" checkboxValues={prices} onUpdate={handleQueryUpdate} />
      </div>
      <div className={styles.itemsContainer}>
        <div className={styles.items}>
          {Boolean(count) && currentData.map((item: ProductProps, i: number) => <ProductItem key={`${item.name}-${i}`} {...item} />)}
        </div>
        {initiallyLoaded && Boolean(!count) && (
          <div className={styles.error}>
            <h4>No images found</h4>
          </div>
        )}
        {totalPages > 1 && (
          <div className={styles.pages}>
            <Pagination currentPage={currentPage} totalPages={totalPages} onPageChange={(page) => setCurrentPage(page)} />
          </div>
        )}
      </div>
    </div>
  )
}

export default ProductsSection;
