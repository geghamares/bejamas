'use client';
import type { ChangeEvent } from 'react';

import type { SortingProps } from 'organisms/ProductsSection/types';
import { SortTypes } from 'constants/fetch';
import { SORTING } from 'constants/icons';
import SVGIcon from 'atoms/SVGIcon';

import styles from './Sorting.module.css';

function Sorting({ values, isAscending, onUpdate }: SortingProps) {

  return (
    <div className={styles.root}>
      <button className={styles.button} onClick={() => onUpdate({ isAscending: !isAscending })}>
        <SVGIcon src={SORTING} alt="sorting" width={15} height={15} />
      </button>
      <span className={styles.label}>Sort By</span>
      <select
        className={styles.select}
        name="Sort By"
        defaultValue={values[0]}
        onChange={
          (e: ChangeEvent<HTMLSelectElement>) => onUpdate({ sortType: SortTypes[e.target.value as keyof typeof SortTypes] })
        }>
        {values.map(value => <option key={value} value={value}>{value}</option>)}
      </select>
    </div>
  );
}

export default Sorting;
