import type { ProductProps } from 'types/product';
import type { SortTypes } from 'constants/fetch';
import type { GetDocumentArgs } from 'utils/getProducts';

export type ProductSectionProps = {
  title: string;
  subtitle: string;
  initialProducts: ProductProps[];
}

export type OnUpdateQuery = (obj: GetDocumentArgs) => void;

export type SortingProps = {
  values: Array<keyof typeof SortTypes>
  onUpdate: OnUpdateQuery;
  isAscending?: boolean;
}

export type BaseFilteringProps = {
  label: string;
  onUpdate: OnUpdateQuery;
}

export interface ICategoryFiltering extends BaseFilteringProps {
  checkboxValues: string[];
}

export type PriceRange = {
  range: { min: number; max: number };
  label: string;
};

export interface IPriceFiltering extends BaseFilteringProps {
  checkboxValues: PriceRange[];
}
