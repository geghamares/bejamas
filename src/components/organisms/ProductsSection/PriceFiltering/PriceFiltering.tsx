'use client';
import { ChangeEvent, useState } from 'react';

import Checkbox from 'atoms/Checkbox';
import type { RangeProps } from 'utils/getProducts';

import styles from './PriceFiltering.module.css';
import type { IPriceFiltering } from '../types';
import { SortTypes } from 'constants/fetch';

function PriceFiltering({ label, checkboxValues, onUpdate }: IPriceFiltering) {
  const [checkedItemLabel, setCheckedItemLabel] = useState('');

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value: RangeProps = JSON.parse(e.target.value);
    const id = e.target.id;
    if (checkedItemLabel !== id) {
      onUpdate({ priceRange: value, sortType: SortTypes.Price });
      setCheckedItemLabel(id)
    } else {
      onUpdate({ priceRange: null });
      setCheckedItemLabel('')
    }
  }

  return (
    <div className={styles.root}>
      <h3 className={styles.label}>{label}</h3>
      <div className={styles.checkboxes}>
        {checkboxValues.map((checkbox) =>
          <Checkbox
            key={checkbox.label}
            id={checkbox.label}
            label={checkbox.label}
            value={JSON.stringify(checkbox.range)}
            isChecked={checkedItemLabel === checkbox.label}
            onChange={e => handleOnChange(e)}
          />
        )}
      </div>
    </div>
  );
}

export default PriceFiltering;
