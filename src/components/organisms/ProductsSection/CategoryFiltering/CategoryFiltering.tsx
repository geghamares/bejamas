'use client';
import { ChangeEvent, useState } from 'react';

import Checkbox from 'atoms/Checkbox';
import type { ICategoryFiltering } from '../types';
import styles from './CategoryFiltering.module.css';

function CategoryFiltering({ label, checkboxValues, onUpdate }: ICategoryFiltering) {
  const [checkedItems, setCheckedItems] = useState<string[]>([]);

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value: string = e.target.value;
    const index = checkedItems.indexOf(value);
    if (index > -1) {
      const removed = checkedItems.filter(item => item !== value)
      setCheckedItems(removed);
      if (!removed.length) {
        onUpdate({ categories: checkboxValues });
      } else {
        onUpdate({ categories: removed });
      }
    } else {
      onUpdate({ categories: [...checkedItems, value]});
      setCheckedItems(prev => ([...prev, value]));
    }
  }

  return (
    <div className={styles.root}>
      <h3 className={styles.label}>{label}</h3>
      <div className={styles.checkboxes}>
        {checkboxValues.map((checkbox) => <Checkbox key={checkbox} label={checkbox} value={checkbox} isChecked={checkedItems.includes(checkbox)} onChange={e => handleOnChange(e)} />)}
      </div>
    </div>
  );
}

export default CategoryFiltering;
