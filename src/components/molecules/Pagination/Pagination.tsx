import type { PaginationProps } from './types';
import styles from './Pagination.module.css';
import SVGIcon from 'atoms/SVGIcon';
import { ARROW_LEFT, ARROW_RIGHT } from 'constants/icons';
import classNames from 'classnames';

function Pagination ({
  currentPage,
  totalPages,
  onPageChange,
}: PaginationProps) {
  const handlePrevPage = () => {
    onPageChange(currentPage - 1);
  };

  const handleNextPage = () => {
    onPageChange(currentPage + 1);
  };

  return (
    <div className={styles.root}>
      {currentPage > 1 && (
        <button className={classNames(styles.button, styles.prev)} onClick={handlePrevPage}>
          <SVGIcon src={ARROW_LEFT} alt="" width={16} height={16} />
        </button>
      )}
      {[...Array(totalPages)].map((_, i) => <span key={`page-${i}`} className={classNames(styles.number, { [styles.active]: currentPage === i+1 })}>{i + 1}</span>)}
      {currentPage < totalPages && (
        <button className={classNames(styles.button, styles.next)} onClick={handleNextPage}>
          <SVGIcon src={ARROW_RIGHT} alt="" width={16} height={16} />
        </button>
      )}
    </div>
  );
}

export default Pagination;
