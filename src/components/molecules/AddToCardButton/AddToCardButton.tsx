'use client'
import { useCallback } from 'react';

import { useAppContext } from 'context/AppContext';
import Button from 'atoms/Button';
import type { ProductProps } from 'types/product';

function AddToCardButton(props: ProductProps & { className?: string }) {
  const { setShoppingCartItems, shoppingCartItems, setIsShoppingCartOpen } = useAppContext();

  const handleClick = useCallback(() => {
    setIsShoppingCartOpen(true);
    if (shoppingCartItems.find((product: ProductProps) => product.name === props.name)) {
      return;
    } else {
      setShoppingCartItems(prevState => [...prevState, props]);
    }
  }, [props, setIsShoppingCartOpen, setShoppingCartItems, shoppingCartItems]);

  return <Button className={props.className} label="ADD TO CART" onClick={() => handleClick()} />
}

export default AddToCardButton;
