import Image from 'next/image';

import AddToCardButton from 'molecules/AddToCardButton';
import { CurrencyOptions } from 'constants/general';
import type { ProductProps } from 'types/product';

import styles from './ProductItem.module.css';

function ProductItem(props: ProductProps) {
  const { name, category, price, image, bestseller, currency } = props;

  return (
    <div className={styles.root}>
      <div className={styles.mediaContainer}>
        <Image className={styles.image} src={image.src} alt={image.alt} sizes="(max-width: 768px) 346px" width={282} height={398} />
        {bestseller && <div className={styles.bestseller}>Best Seller</div>}
        <AddToCardButton className={styles.button} {...props} />
      </div>
      <div className={styles.textContainer}>
        <span className={styles.category}>{category}</span>
        <h3 className={styles.name}>{name}</h3>
        <span className={styles.price}>{`${CurrencyOptions[currency]}${price}`}</span>
      </div>
    </div>
  )
}

export default ProductItem
