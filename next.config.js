/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  images: {
    formats: ['image/webp'],
    domains: ['images.pexels.com', 'firebasestorage.googleapis.com'],
  },
}

module.exports = nextConfig
